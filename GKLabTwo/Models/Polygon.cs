﻿using GKLabTwo.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GKLabTwo.Models
{
    internal abstract class Polygon
    {
        public abstract List<Edge> Edges { get; }
        public abstract List<Vertex> Vertecies { get; }

        internal bool Inside(Point location) => IsInside(location);

        internal void Trasform(int x, int y)
        {
            foreach (var vertex in Vertecies)
            {
                vertex.Location = new Point(vertex.Location.X + x, vertex.Location.Y + y);
            }
        }



        /// <summary>
        /// Ref https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/ 
        /// Returns true if the point p lies inside the polygon[] with n vertices 
        /// </summary>
        bool IsInside(Point p)
        {
            Point extreme = new Point(100000, p.Y);

            // Count intersections of the above line with sides of polygon 
            int count = 0;
            for (int i = 0; i < Vertecies.Count; i++)
            {
                int next = (i + 1) % Vertecies.Count;

                // Check if the line segment from 'p' to 'extreme' intersects 
                // with the line segment from 'polygon[i]' to 'polygon[next]' 
                if (DoIntersect(Vertecies[i].Location, Vertecies[next].Location, p, extreme))
                {
                    if (Orientation(Vertecies[i].Location, p, Vertecies[next].Location) == 0)
                        return OnSegment(Vertecies[i].Location, p, Vertecies[next].Location);

                    count++;
                }
                i = next;
            }

            // Return true if count is odd, false otherwise 
            return count % 2 == 1;



            bool OnSegment(Point p1, Point p2, Point r)
            {
                if (p2.X <= Math.Max(p1.X, r.X) && p2.X >= Math.Min(p1.X, r.X) &&
                        p2.Y <= Math.Max(p1.Y, r.Y) && p2.Y >= Math.Min(p1.Y, r.Y))
                    return true;
                return false;
            }

            // To find orientation of ordered triplet (p, q, r). 
            // The function returns following values 
            // 0 --> p, q and r are colinear 
            // 1 --> Clockwise 
            // 2 --> Counterclockwise 
            int Orientation(Point p1, Point p2, Point r)
            {
                int val = (p2.Y - p1.Y) * (r.X - p2.X) -
                          (p2.X - p1.X) * (r.Y - p2.Y);

                if (val == 0) return 0;  // colinear 
                return (val > 0) ? 1 : 2; // clock or counterclock wise 
            }

            // The function that returns true if line segment 'p1q1' 
            // and 'p2q2' intersect. 
            bool DoIntersect(Point p1, Point q1, Point p2, Point q2)
            {
                // Find the four orientations needed for general and 
                // special cases 
                int o1 = Orientation(p1, q1, p2);
                int o2 = Orientation(p1, q1, q2);
                int o3 = Orientation(p2, q2, p1);
                int o4 = Orientation(p2, q2, q1);

                // General case 
                if (o1 != o2 && o3 != o4)
                    return true;

                // Special Cases 
                // p1, q1 and p2 are colinear and p2 lies on segment p1q1 
                if (o1 == 0 && OnSegment(p1, p2, q1)) return true;

                // p1, q1 and p2 are colinear and q2 lies on segment p1q1 
                if (o2 == 0 && OnSegment(p1, q2, q1)) return true;

                // p2, q2 and p1 are colinear and p1 lies on segment p2q2 
                if (o3 == 0 && OnSegment(p2, p1, q2)) return true;

                // p2, q2 and q1 are colinear and q1 lies on segment p2q2 
                if (o4 == 0 && OnSegment(p2, q1, q2)) return true;

                return false; // Doesn't fall in any of the above cases 
            }
        }
    }
}