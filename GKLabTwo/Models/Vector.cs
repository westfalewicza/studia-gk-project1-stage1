﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo.Models
{
    class Vector
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public Vector(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }
        public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y + Z * Z);
        }

        public Vector Normalized()
        {
            var length = Length();
            return new Vector(X / length, Y / length, Z / length);
        }

        internal Color ToColor()
        {
            try
            {
                return Color.FromArgb((int)(X * 255), (int)(Y * 255), (int)(Z * 255));
            }
            catch {
                return Color.Black;
            }
        }

        static internal Vector FromColor(Color color)
        {
            return new Vector(color.R/ 255f, color.G / 255f, color.B / 255f);
        }

        public static Vector UnitZ { get; } = new Vector(0, 0, 1);
    }
}
