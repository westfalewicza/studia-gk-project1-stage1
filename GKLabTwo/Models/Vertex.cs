﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo.Models
{
    public class Vertex
    {
        public Point Location { get; set; }

        public Vertex Next => Forward.End;
        public Edge Forward { get; set; }

        public Vertex Previous => Back.Start;
        public Edge Back { get; set; }

        public string Name { get; private set; }

        public Vertex()
        {
            Name = new Random().Next(0, 999).ToString();
        }
    }
}
