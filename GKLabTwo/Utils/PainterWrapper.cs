﻿using GKLabTwo.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GKLabTwo.Utils
{
    class PainterWrapper
    {
        readonly Bitmap bitmap;
        readonly DirectBitmap directBitmap;
        readonly Graphics graphics;
        readonly int width;
        readonly int height;

        public PainterWrapper(Bitmap bitmap, DirectBitmap directBitmap)
        {
            this.bitmap = bitmap;
            this.directBitmap = directBitmap;
            graphics = Graphics.FromImage(bitmap);
            width = bitmap.Width;
            height = bitmap.Height;

            PolygonColor = Config.FillColor;
            LightColor = Color.White;
            MovingLight = false;
            m = 5;
        }

        public bool MovingLight;
        public Vector LightLocation;
        public Color LightColor;
        public Control ToRefresh;

        private float m;
        public float M
        {
            get { return m; }
            set
            {
                if (m == 0)
                    m = 0.1f;
                else
                    m = value;
            }
        }

        private float[,] heightMapProcessed;
        private Bitmap heightMap;
        public Bitmap HeightMap
        {
            get { return heightMap; }
            set
            {
                heightMap = value;
                if (value == null)
                    return;

                heightMapProcessed = new float[width, height];
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        heightMapProcessed[x, y] = Vector.FromColor(heightMap.GetPixel(x, y)).Length();
                    }
                }
            }
        }

        private Vector[,] ShapeVectorized;
        private Bitmap shape;
        public Bitmap Shape
        {
            get { return shape; }
            set
            {
                shape = value;
                if (value == null)
                    return;

                ShapeVectorized = new Vector[width, height];
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        var Rx = (shape.GetPixel(x, y).R - 127) / 255f;
                        var Ry = (shape.GetPixel(x, y).G - 127) / 255f;
                        var Rz = shape.GetPixel(x, y).B / 255f;

                        ShapeVectorized[x, y] = new Vector(Rx, Ry, Rz);
                    }
                }
            }
        }

        private Vector polygonColorProcessed;
        private Color polygonColor;
        public Color PolygonColor
        {
            get { return polygonColor; }
            set
            {
                polygonColor = value;
                polygonColorProcessed = Vector.FromColor(polygonColor);
            }
        }

        private Vector[,] polygonTextureProcessed;
        private Bitmap polygonTexture;
        public Bitmap PolygonTexture
        {
            get
            {
                return polygonTexture;
            }
            set
            {
                polygonTexture = value;
                if (value == null)
                    return;

                polygonTextureProcessed = new Vector[width, height];
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        polygonTextureProcessed[x, y] = Vector.FromColor(polygonTexture.GetPixel(x, y));
                    }
                }

            }
        }

        private Vector GetPolygonColor(int x, int y) =>
            PolygonTexture != null ? polygonTextureProcessed[x, y] : polygonColorProcessed;

        private Vector GetShape(int x, int y) =>
            Shape != null ? ShapeVectorized[x, y] : Vector.UnitZ;

        private float GetDistortion(int x, int y) =>
            HeightMap != null ? heightMapProcessed[x, y] : 0;

        private Color GetLightColor() =>
            LightColor;

        private Vector GetLightVector(int x, int y) =>
            MovingLight
            ? new Vector(LightLocation.X - x, LightLocation.Y - y, LightLocation.Z).Normalized()
            : Vector.UnitZ;

        private float GetLighting(int x, int y)
        {
            Vector N = GetShape(x, y);

            float dhx = GetDistortion(x + 1, y) - GetDistortion(x, y);
            float dhy = GetDistortion(x, y + 1) - GetDistortion(x, y);
            Vector D = new Vector(dhx, dhy, -(N.X * dhx + N.Y * dhy));
            Vector Ndistorted = new Vector(D.X + N.X, D.Y + N.Y, D.Z + N.Z);
            Ndistorted = Ndistorted.Normalized();

            Vector L = GetLightVector(x, y);
            Vector R = new Vector(Ndistorted.X * 2 - L.X, Ndistorted.Y * 2 - L.Y, Ndistorted.Z * 2 - L.Z);

            //return Ndistorted.X * L.X + Ndistorted.Y * L.Y + Ndistorted.Z * L.Z;
            return (float)Math.Pow(R.Normalized().Z, M);
        }

        public void PaintPolygonFlood(params Polygon[] polygons)
        {
            var whitebrush = new SolidBrush(Color.FromKnownColor(KnownColor.White));

            var Floodrect = new Rectangle(0, 0, width, height);
            graphics.FillRectangle(whitebrush, Floodrect);

            foreach (var polygon in polygons)
                PaintPolygon(polygon);
        }

        public void PaintPolygon(Polygon polygon)
        {
            var color = Config.EdgeColor;
            var brush = new SolidBrush(color);
            var pen = new Pen(brush, Config.PenWidth);

            FillPolygon(polygon);

            foreach (var edge in polygon.Edges)
            {
                graphics.DrawLine(pen, edge.V1, edge.V2);
                var font = new Font(Config.FontName, Config.FontSize);
                graphics.DrawString(edge.Name, font, new SolidBrush(Config.LabelColor), Helper.MidPoint(edge.V1, edge.V2));
            }
        }

        /// <summary>
        /// Ref: https://eduinf.waw.pl/inf/utils/002_roz/2008_22.php
        /// </summary>
        public void FillPolygon(Polygon polygon)
        {
            int yMin = int.MaxValue;
            int yMax = int.MinValue;
            var buckets = new Dictionary<int, List<HelpEdge>>(polygon.Edges.Count);

            foreach (Edge edge in polygon.Edges)
            {
                var helpEdge = HelpEdge.FromEdge(edge);
                if (helpEdge.V1.Y < yMin)
                    yMin = helpEdge.V1.Y;
                if (helpEdge.V2.Y > yMax)
                    yMax = helpEdge.V2.Y;

                if (helpEdge.CurrentX < 0)
                    continue; //Horizontal

                if (buckets.ContainsKey(helpEdge.V1.Y))
                    buckets[helpEdge.V1.Y].Add(helpEdge);
                else
                    buckets.Add(helpEdge.V1.Y, new List<HelpEdge>() { helpEdge });
            }

            var activeEdges = new List<HelpEdge>();
            for (int y = yMin; y < yMax; y++)
            {
                for (int i = 0; i < activeEdges.Count; i++)
                {
                    if (activeEdges[i].V2.Y == y)
                    {
                        activeEdges.RemoveAt(i);
                        i--;    //Process next element at the same position
                    }
                }
                bool newEdges = buckets.TryGetValue(y, out var list);
                if (newEdges)
                    activeEdges.AddRange(list);
                activeEdges.OrderBy(he => he.CurrentX);

                bool selector = false;
                int x2 = int.MinValue;
                int x1 = int.MinValue;

                for (int i = 0; i < activeEdges.Count; i++)
                {
                    if (selector)
                    {
                        x2 = (int)Math.Floor(activeEdges[i].CurrentX);
                        if (x1 <= x2)
                        {
                            for (int xi = x1; xi <= x2; xi++)
                                ColorPixel(xi, y);
                        }
                        else
                        {
                            for (int xi = x2; xi <= x1; xi++)
                                ColorPixel(xi, y);
                        }

                        if (Config.RefreshAfterEachLine && ToRefresh != null)
                            ToRefresh.Refresh();
                    }
                    else
                    {
                        x1 = (int)Math.Ceiling(activeEdges[i].CurrentX);
                    }
                    selector = !selector;
                    activeEdges[i].CurrentX += activeEdges[i].Dx;
                }
            }
        }

        private void ColorPixel(int x, int y)
        {
            Color lightColor = GetLightColor();
            Vector pixelColor = GetPolygonColor(x, y);
            float lightCoefficient = GetLighting(x, y);
            if (lightCoefficient < 0) lightCoefficient = 0;
            var r = lightColor.R / 255f * pixelColor.X * lightCoefficient;
            var g = lightColor.G / 255f * pixelColor.Y * lightCoefficient;
            var b = lightColor.B / 255f * pixelColor.Z * lightCoefficient;
            var vector = new Vector(r, g, b);

            directBitmap.SetPixel(x, y, vector.ToColor());
        }

        class HelpEdge
        {
            public Point V1;
            public Point V2;
            public float Dx;
            public float CurrentX;

            public static HelpEdge FromEdge(Edge edge)
            {
                var higherIsV1 = edge.V1.Y < edge.V2.Y;
                var helpEdge = new HelpEdge()
                {
                    V1 = higherIsV1 ? edge.V1 : edge.V2,
                    V2 = higherIsV1 ? edge.V2 : edge.V1
                };
                if (edge.V1.Y == edge.V2.Y) //Horizontal
                {
                    helpEdge.Dx = -1;
                    helpEdge.CurrentX = -1;
                }
                else
                {
                    helpEdge.Dx = (float)(helpEdge.V2.X - helpEdge.V1.X) / (float)(helpEdge.V2.Y - helpEdge.V1.Y);
                    helpEdge.CurrentX = helpEdge.V1.X;
                }
                return helpEdge;
            }
        }
    }
}
