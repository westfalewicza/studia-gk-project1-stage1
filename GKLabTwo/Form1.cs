﻿using GKLabTwo.Models;
using GKLabTwo.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace GKLabTwo
{
    public partial class MainWindow : Form
    {
        private Timer lightShifter;
        private float angle;

        private int DesiredWidth { get { return Painter.Width; } }
        private int DesiredHeight { get { return Painter.Height; } }
        private Polygon polygon1;
        private Polygon polygon2;
        private Bitmap bitmap;
        private DirectBitmap directBitmap;
        private PainterWrapper painterWrapper;

        private Point prevMouseLocation;
        private Polygon polygonToMove;

        public MainWindow()
        {
            InitializeComponent();
            angle = 0;
            lightShifter = new Timer();
            lightShifter.Interval = 1000 / 30;
            lightShifter.Tick += ShiftLight;
        }

        private void ShiftLight(object sender, EventArgs e)
        {
            angle += Config.LightSpeed;
            if (angle > Math.PI * 2)
                angle -= (float)Math.PI * 2;
            painterWrapper.LightLocation = new Vector(
                DesiredWidth / 2 + Config.LightFlatDistance * (float)Math.Sin(angle),
                DesiredHeight / 2 + Config.LightFlatDistance * (float)Math.Cos(angle),
                Config.LightHeight);
            Repaint();
        }

        private void Repaint()
        {
            try
            {
                painterWrapper.PaintPolygonFlood(polygon1, polygon2);
                Painter.Refresh();
            }
            catch(Exception e)
            {
                Debug.Write(e);
                this.Text = "Exceptional error :)";
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            directBitmap = new DirectBitmap(DesiredWidth, DesiredHeight);
            bitmap = directBitmap.Bitmap;
            Painter.BackgroundImage = bitmap;
            DoubleBuffered = true;
            painterWrapper = new PainterWrapper(directBitmap.Bitmap, directBitmap)
            {
                ToRefresh = Painter
            };

            if (Config.BigMamaTriangle)
            {
                var p1 = new Point(50, 50);
                var p2 = new Point(50, 60);
                var p3 = new Point(60, 50);
                this.polygon1 = new Square();
                p1 = new Point(100, 100);
                p2 = new Point(100, 600);
                p3 = new Point(500, 50);
                this.polygon2 = new Triangle(p1, p2, p3);
            }
            else
            {

                var p1 = new Point(100, 100);
                var p2 = new Point(200, 50);
                var p3 = new Point(200, 150);
                this.polygon1 = new Triangle(p1, p2, p3);
                p1 = new Point(200, 200);
                p2 = new Point(300, 150);
                p3 = new Point(300, 250);
                this.polygon2 = new Triangle(p1, p2, p3);
            }
            Repaint();
        }

        private void Painter_MouseMove(object sender, MouseEventArgs e)
        {
            if (polygonToMove != null && e.Location != prevMouseLocation)
            {
                int x = e.Location.X - prevMouseLocation.X;
                int y = e.Location.Y - prevMouseLocation.Y;
                polygonToMove.Trasform(x, y);
                prevMouseLocation = e.Location;
                Repaint();
            }
        }

        private void Painter_MouseUp(object sender, MouseEventArgs e)
        {
            polygonToMove = null;
            prevMouseLocation = new Point(-1, -1);
        }

        private void Painter_MouseDown(object sender, MouseEventArgs e)
        {
            if (polygon1.Inside(e.Location))
            {
                polygonToMove = polygon1;
                prevMouseLocation = e.Location;
            }
            else if (polygon2.Inside(e.Location))
            {
                polygonToMove = polygon2;
                prevMouseLocation = e.Location;
            }
        }

        private void PickColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                painterWrapper.PolygonTexture = null;
                painterWrapper.PolygonColor = colorDialog1.Color;
                ColorLabel.Text = $"{colorDialog1.Color.R}, {colorDialog1.Color.G}, {colorDialog1.Color.B}";
                ColorLabel.ForeColor = colorDialog1.Color;
                Repaint();
            }
        }

        private void PickTexture_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (var stream = openFileDialog1.OpenFile())
                {
                    var bitmap = new Bitmap(stream);
                    Bitmap resized = new Bitmap(bitmap, DesiredWidth, DesiredHeight);
                    painterWrapper.PolygonTexture = resized;
                }
                ColorLabel.ForeColor = Color.Black;
                ColorLabel.Text = Path.GetFileName(openFileDialog1.FileName);
                Repaint();
            }
        }

        private void PickNormalFlat_Click(object sender, EventArgs e)
        {
            painterWrapper.Shape = null;
            NormalLabel.Text = "Flat";
            Repaint();
        }

        private void PickNormalMap_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (var stream = openFileDialog1.OpenFile())
                {
                    var bitmap = new Bitmap(stream);
                    Bitmap resized = new Bitmap(bitmap, DesiredWidth, DesiredHeight);
                    painterWrapper.Shape = resized;
                }
                NormalLabel.Text = Path.GetFileName(openFileDialog1.FileName);
                Repaint();
            }
        }

        private void PickDistortionNone_Click(object sender, EventArgs e)
        {
            painterWrapper.HeightMap = null;
            DistortionLabel.Text = "None";
            Repaint();
        }

        private void PickDistortionMap_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (var stream = openFileDialog1.OpenFile())
                {
                    var bitmap = new Bitmap(stream);
                    Bitmap resized = new Bitmap(bitmap, DesiredWidth, DesiredHeight);
                    painterWrapper.HeightMap = resized;
                }
                DistortionLabel.Text = Path.GetFileName(openFileDialog1.FileName);
                Repaint();
            }
        }

        private void PickLightColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                painterWrapper.LightColor = colorDialog1.Color;
                LightColorLabel.Text = $"{colorDialog1.Color.R}, {colorDialog1.Color.G}, {colorDialog1.Color.B}";
                LightColorLabel.ForeColor = colorDialog1.Color;
                Repaint();
            }
        }




        private void Still_CheckedChanged(object sender, EventArgs e) => ProcessLight();
        private void Moving_CheckedChanged(object sender, EventArgs e) => ProcessLight();
        private void ProcessLight()
        {
            if (Moving.Checked)
            {
                lightShifter.Start();
                painterWrapper.MovingLight = true;
                painterWrapper.LightLocation = new Vector(
                 DesiredWidth / 2 + Config.LightFlatDistance * (float)Math.Sin(angle),
                 DesiredHeight / 2 + Config.LightFlatDistance * (float)Math.Cos(angle),
                 Config.LightHeight);
                Repaint();
            }
            else
            {
                lightShifter.Stop();
                painterWrapper.MovingLight = false;
                Repaint();
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            var m = trackBar1.Value;
            painterWrapper.M = (float)m;
            Repaint();
        }
    }
}
