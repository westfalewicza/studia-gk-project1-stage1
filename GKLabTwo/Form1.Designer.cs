﻿namespace GKLabTwo
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Splitter = new System.Windows.Forms.SplitContainer();
            this.PhongGroup = new System.Windows.Forms.GroupBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.LightVector = new System.Windows.Forms.GroupBox();
            this.Moving = new System.Windows.Forms.RadioButton();
            this.Still = new System.Windows.Forms.RadioButton();
            this.LightColorBox = new System.Windows.Forms.GroupBox();
            this.PickLightColor = new System.Windows.Forms.Button();
            this.LightColorLabel = new System.Windows.Forms.Label();
            this.DistortionGroup = new System.Windows.Forms.GroupBox();
            this.PickDistortionMap = new System.Windows.Forms.Button();
            this.PickDistortionNone = new System.Windows.Forms.Button();
            this.DistortionLabel = new System.Windows.Forms.Label();
            this.NormalGroup = new System.Windows.Forms.GroupBox();
            this.PickNormalMap = new System.Windows.Forms.Button();
            this.PickNormalFlat = new System.Windows.Forms.Button();
            this.NormalLabel = new System.Windows.Forms.Label();
            this.ColorTextureGroup = new System.Windows.Forms.GroupBox();
            this.PickTexture = new System.Windows.Forms.Button();
            this.PickColor = new System.Windows.Forms.Button();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.Painter = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Splitter)).BeginInit();
            this.Splitter.Panel1.SuspendLayout();
            this.Splitter.Panel2.SuspendLayout();
            this.Splitter.SuspendLayout();
            this.PhongGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.LightVector.SuspendLayout();
            this.LightColorBox.SuspendLayout();
            this.DistortionGroup.SuspendLayout();
            this.NormalGroup.SuspendLayout();
            this.ColorTextureGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Painter)).BeginInit();
            this.SuspendLayout();
            // 
            // Splitter
            // 
            this.Splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Splitter.Location = new System.Drawing.Point(0, 0);
            this.Splitter.Name = "Splitter";
            // 
            // Splitter.Panel1
            // 
            this.Splitter.Panel1.Controls.Add(this.PhongGroup);
            this.Splitter.Panel1.Controls.Add(this.LightVector);
            this.Splitter.Panel1.Controls.Add(this.LightColorBox);
            this.Splitter.Panel1.Controls.Add(this.DistortionGroup);
            this.Splitter.Panel1.Controls.Add(this.NormalGroup);
            this.Splitter.Panel1.Controls.Add(this.ColorTextureGroup);
            // 
            // Splitter.Panel2
            // 
            this.Splitter.Panel2.Controls.Add(this.Painter);
            this.Splitter.Size = new System.Drawing.Size(1095, 943);
            this.Splitter.SplitterDistance = 149;
            this.Splitter.TabIndex = 0;
            // 
            // PhongGroup
            // 
            this.PhongGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PhongGroup.Controls.Add(this.trackBar1);
            this.PhongGroup.Location = new System.Drawing.Point(10, 508);
            this.PhongGroup.Name = "PhongGroup";
            this.PhongGroup.Size = new System.Drawing.Size(129, 83);
            this.PhongGroup.TabIndex = 6;
            this.PhongGroup.TabStop = false;
            this.PhongGroup.Text = "Phong M value";
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.Location = new System.Drawing.Point(3, 21);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(120, 56);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickFrequency = 0;
            this.trackBar1.Value = 1;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // LightVector
            // 
            this.LightVector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LightVector.Controls.Add(this.Moving);
            this.LightVector.Controls.Add(this.Still);
            this.LightVector.Location = new System.Drawing.Point(10, 429);
            this.LightVector.Name = "LightVector";
            this.LightVector.Size = new System.Drawing.Size(129, 73);
            this.LightVector.TabIndex = 5;
            this.LightVector.TabStop = false;
            this.LightVector.Text = "Light Source";
            // 
            // Moving
            // 
            this.Moving.AutoSize = true;
            this.Moving.Location = new System.Drawing.Point(6, 48);
            this.Moving.Name = "Moving";
            this.Moving.Size = new System.Drawing.Size(74, 21);
            this.Moving.TabIndex = 2;
            this.Moving.TabStop = true;
            this.Moving.Text = "Moving";
            this.Moving.UseVisualStyleBackColor = true;
            this.Moving.CheckedChanged += new System.EventHandler(this.Moving_CheckedChanged);
            // 
            // Still
            // 
            this.Still.AutoSize = true;
            this.Still.Checked = true;
            this.Still.Location = new System.Drawing.Point(6, 21);
            this.Still.Name = "Still";
            this.Still.Size = new System.Drawing.Size(51, 21);
            this.Still.TabIndex = 1;
            this.Still.TabStop = true;
            this.Still.Text = "Still";
            this.Still.UseVisualStyleBackColor = true;
            this.Still.CheckedChanged += new System.EventHandler(this.Still_CheckedChanged);
            // 
            // LightColorBox
            // 
            this.LightColorBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LightColorBox.Controls.Add(this.PickLightColor);
            this.LightColorBox.Controls.Add(this.LightColorLabel);
            this.LightColorBox.Location = new System.Drawing.Point(10, 350);
            this.LightColorBox.Name = "LightColorBox";
            this.LightColorBox.Size = new System.Drawing.Size(129, 73);
            this.LightColorBox.TabIndex = 4;
            this.LightColorBox.TabStop = false;
            this.LightColorBox.Text = "Light Color";
            // 
            // PickLightColor
            // 
            this.PickLightColor.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickLightColor.Location = new System.Drawing.Point(3, 41);
            this.PickLightColor.Name = "PickLightColor";
            this.PickLightColor.Size = new System.Drawing.Size(123, 23);
            this.PickLightColor.TabIndex = 1;
            this.PickLightColor.Text = "Pick Light color";
            this.PickLightColor.UseVisualStyleBackColor = true;
            this.PickLightColor.Click += new System.EventHandler(this.PickLightColor_Click);
            // 
            // LightColorLabel
            // 
            this.LightColorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LightColorLabel.ForeColor = System.Drawing.Color.White;
            this.LightColorLabel.Location = new System.Drawing.Point(3, 18);
            this.LightColorLabel.Name = "LightColorLabel";
            this.LightColorLabel.Size = new System.Drawing.Size(123, 23);
            this.LightColorLabel.TabIndex = 0;
            this.LightColorLabel.Text = "255, 255, 255";
            this.LightColorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DistortionGroup
            // 
            this.DistortionGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DistortionGroup.Controls.Add(this.PickDistortionMap);
            this.DistortionGroup.Controls.Add(this.PickDistortionNone);
            this.DistortionGroup.Controls.Add(this.DistortionLabel);
            this.DistortionGroup.Location = new System.Drawing.Point(10, 240);
            this.DistortionGroup.Name = "DistortionGroup";
            this.DistortionGroup.Size = new System.Drawing.Size(129, 100);
            this.DistortionGroup.TabIndex = 3;
            this.DistortionGroup.TabStop = false;
            this.DistortionGroup.Text = "Distortion";
            // 
            // PickDistortionMap
            // 
            this.PickDistortionMap.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickDistortionMap.Location = new System.Drawing.Point(3, 64);
            this.PickDistortionMap.Name = "PickDistortionMap";
            this.PickDistortionMap.Size = new System.Drawing.Size(123, 23);
            this.PickDistortionMap.TabIndex = 2;
            this.PickDistortionMap.Text = "Pick height map";
            this.PickDistortionMap.UseVisualStyleBackColor = true;
            this.PickDistortionMap.Click += new System.EventHandler(this.PickDistortionMap_Click);
            // 
            // PickDistortionNone
            // 
            this.PickDistortionNone.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickDistortionNone.Location = new System.Drawing.Point(3, 41);
            this.PickDistortionNone.Name = "PickDistortionNone";
            this.PickDistortionNone.Size = new System.Drawing.Size(123, 23);
            this.PickDistortionNone.TabIndex = 1;
            this.PickDistortionNone.Text = "None";
            this.PickDistortionNone.UseVisualStyleBackColor = true;
            this.PickDistortionNone.Click += new System.EventHandler(this.PickDistortionNone_Click);
            // 
            // DistortionLabel
            // 
            this.DistortionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.DistortionLabel.Location = new System.Drawing.Point(3, 18);
            this.DistortionLabel.Name = "DistortionLabel";
            this.DistortionLabel.Size = new System.Drawing.Size(123, 23);
            this.DistortionLabel.TabIndex = 0;
            this.DistortionLabel.Text = "None";
            this.DistortionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NormalGroup
            // 
            this.NormalGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NormalGroup.Controls.Add(this.PickNormalMap);
            this.NormalGroup.Controls.Add(this.PickNormalFlat);
            this.NormalGroup.Controls.Add(this.NormalLabel);
            this.NormalGroup.Location = new System.Drawing.Point(10, 120);
            this.NormalGroup.Name = "NormalGroup";
            this.NormalGroup.Size = new System.Drawing.Size(129, 100);
            this.NormalGroup.TabIndex = 2;
            this.NormalGroup.TabStop = false;
            this.NormalGroup.Text = "Shape";
            // 
            // PickNormalMap
            // 
            this.PickNormalMap.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickNormalMap.Location = new System.Drawing.Point(3, 64);
            this.PickNormalMap.Name = "PickNormalMap";
            this.PickNormalMap.Size = new System.Drawing.Size(123, 23);
            this.PickNormalMap.TabIndex = 2;
            this.PickNormalMap.Text = "Pick Map";
            this.PickNormalMap.UseVisualStyleBackColor = true;
            this.PickNormalMap.Click += new System.EventHandler(this.PickNormalMap_Click);
            // 
            // PickNormalFlat
            // 
            this.PickNormalFlat.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickNormalFlat.Location = new System.Drawing.Point(3, 41);
            this.PickNormalFlat.Name = "PickNormalFlat";
            this.PickNormalFlat.Size = new System.Drawing.Size(123, 23);
            this.PickNormalFlat.TabIndex = 1;
            this.PickNormalFlat.Text = "Flat";
            this.PickNormalFlat.UseVisualStyleBackColor = true;
            this.PickNormalFlat.Click += new System.EventHandler(this.PickNormalFlat_Click);
            // 
            // NormalLabel
            // 
            this.NormalLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.NormalLabel.Location = new System.Drawing.Point(3, 18);
            this.NormalLabel.Name = "NormalLabel";
            this.NormalLabel.Size = new System.Drawing.Size(123, 23);
            this.NormalLabel.TabIndex = 0;
            this.NormalLabel.Text = "Flat";
            this.NormalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ColorTextureGroup
            // 
            this.ColorTextureGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ColorTextureGroup.Controls.Add(this.PickTexture);
            this.ColorTextureGroup.Controls.Add(this.PickColor);
            this.ColorTextureGroup.Controls.Add(this.ColorLabel);
            this.ColorTextureGroup.Location = new System.Drawing.Point(10, 10);
            this.ColorTextureGroup.Name = "ColorTextureGroup";
            this.ColorTextureGroup.Size = new System.Drawing.Size(129, 100);
            this.ColorTextureGroup.TabIndex = 0;
            this.ColorTextureGroup.TabStop = false;
            this.ColorTextureGroup.Text = "Color/Texture";
            // 
            // PickTexture
            // 
            this.PickTexture.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickTexture.Location = new System.Drawing.Point(3, 64);
            this.PickTexture.Name = "PickTexture";
            this.PickTexture.Size = new System.Drawing.Size(123, 23);
            this.PickTexture.TabIndex = 2;
            this.PickTexture.Text = "Pick Texture";
            this.PickTexture.UseVisualStyleBackColor = true;
            this.PickTexture.Click += new System.EventHandler(this.PickTexture_Click);
            // 
            // PickColor
            // 
            this.PickColor.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickColor.Location = new System.Drawing.Point(3, 41);
            this.PickColor.Name = "PickColor";
            this.PickColor.Size = new System.Drawing.Size(123, 23);
            this.PickColor.TabIndex = 1;
            this.PickColor.Text = "PickColor";
            this.PickColor.UseVisualStyleBackColor = true;
            this.PickColor.Click += new System.EventHandler(this.PickColor_Click);
            // 
            // ColorLabel
            // 
            this.ColorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ColorLabel.ForeColor = System.Drawing.Color.Red;
            this.ColorLabel.Location = new System.Drawing.Point(3, 18);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(123, 23);
            this.ColorLabel.TabIndex = 0;
            this.ColorLabel.Text = "255, 0, 0";
            this.ColorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Painter
            // 
            this.Painter.BackColor = System.Drawing.Color.White;
            this.Painter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Painter.Location = new System.Drawing.Point(0, 0);
            this.Painter.Name = "Painter";
            this.Painter.Size = new System.Drawing.Size(942, 943);
            this.Painter.TabIndex = 0;
            this.Painter.TabStop = false;
            this.Painter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Painter_MouseDown);
            this.Painter.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Painter_MouseMove);
            this.Painter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Painter_MouseUp);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1095, 943);
            this.Controls.Add(this.Splitter);
            this.Name = "MainWindow";
            this.Text = "Westfalewicza GKlab2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.Splitter.Panel1.ResumeLayout(false);
            this.Splitter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Splitter)).EndInit();
            this.Splitter.ResumeLayout(false);
            this.PhongGroup.ResumeLayout(false);
            this.PhongGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.LightVector.ResumeLayout(false);
            this.LightVector.PerformLayout();
            this.LightColorBox.ResumeLayout(false);
            this.DistortionGroup.ResumeLayout(false);
            this.NormalGroup.ResumeLayout(false);
            this.ColorTextureGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Painter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer Splitter;
        private System.Windows.Forms.PictureBox Painter;
        private System.Windows.Forms.GroupBox ColorTextureGroup;
        private System.Windows.Forms.Button PickTexture;
        private System.Windows.Forms.Button PickColor;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.GroupBox LightColorBox;
        private System.Windows.Forms.Button PickLightColor;
        private System.Windows.Forms.Label LightColorLabel;
        private System.Windows.Forms.GroupBox DistortionGroup;
        private System.Windows.Forms.Button PickDistortionMap;
        private System.Windows.Forms.Button PickDistortionNone;
        private System.Windows.Forms.Label DistortionLabel;
        private System.Windows.Forms.GroupBox NormalGroup;
        private System.Windows.Forms.Button PickNormalMap;
        private System.Windows.Forms.Button PickNormalFlat;
        private System.Windows.Forms.Label NormalLabel;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.GroupBox LightVector;
        private System.Windows.Forms.RadioButton Moving;
        private System.Windows.Forms.RadioButton Still;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox PhongGroup;
        private System.Windows.Forms.TrackBar trackBar1;
    }
}

